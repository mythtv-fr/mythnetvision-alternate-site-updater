#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

__title__ ="MythNetvisionAlternateSiteUpdater";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.1"
__usage__ ='''
(Option Help)
> %(file)s -h
Usage: %(file)s [option]
Version: %(version)s Author: %(author)s

Options:
  --help            show this help message and exit
'''%{'file':__file__, 'version':__version__,'author':__author__}


import os,sys,re,time
from xml.dom import minidom
from datetime import datetime
from rfc822 import parsedate
import MySQLdb

reload(sys)
sys.setdefaultencoding('utf-8')

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

#init
grabberDir = '/usr/share/mythtv/internetcontent/'

def MinidomNode_getTextByTagName(self,tagName) :
    try :
        text = self.getElementsByTagName(tagName)[0].firstChild.data
    except :
        return ''
    else :
        return text
setattr(minidom.Node,'getTextByTagName',MinidomNode_getTextByTagName)


def MinidomNode_getPath(self) :
    path = []
    parent = self.parentNode
    while (parent.tagName == 'directory') :
        if parent.hasAttribute('name') :
            path.append(parent.getAttribute('name'))
        else :
            path.append('')
        parent = parent.parentNode
    path.reverse()
    return '/'.join(path)
setattr(minidom.Node,'getPath',MinidomNode_getPath)


def MinidomNode_getPathThumb(self) :
    parent = self.parentNode
    if parent.tagName == 'directory' :
        if parent.hasAttribute('thumbnail') :
            return parent.getAttribute('thumbnail')
        else :
            try :
                return self.getElementsByTagName('media:thumbnail')[0].getAttribute('url')
            except :
                return ''
    return ''
setattr(minidom.Node,'getPathThumb',MinidomNode_getPathThumb)


def MinidomNode_getDate(self) :
    try:
        pubDate = self.getElementsByTagName('pubDate')[0].firstChild.data
    except:
        sqlDate = ''
    else:
        sqlDate = time.strftime('%Y-%m-%d %H:%M:%S',parsedate(pubDate))
    return sqlDate
setattr(minidom.Node,'getDate',MinidomNode_getDate)


def MinidomNode_getMediaContent(self) :
    mediaContent = {'url':'', 'length':'0', 'duration':'', 'width':'0','height':'0','lang':''}
    mediaContentNodes = self.getElementsByTagName('media:content')
    if len(mediaContentNodes) != 0 :
        for attr in mediaContent.keys() :
            if mediaContentNodes[0].hasAttribute(attr) :
                mediaContent[attr] = mediaContentNodes[0].getAttribute(attr)
    return mediaContent
setattr(minidom.Node,'getMediaContent',MinidomNode_getMediaContent)


def MinidomNode_getMediaThumbnail(self) :
    mediaThumbnail = {'url':''}
    mediaThumbnailNodes = self.getElementsByTagName('media:thumbnail')
    if len(mediaThumbnailNodes) != 0 :
        for attr in mediaThumbnail.keys() :
            if mediaThumbnailNodes[0].hasAttribute(attr) :
                mediaThumbnail[attr] = mediaThumbnailNodes[0].getAttribute(attr)
    return mediaThumbnail
setattr(minidom.Node,'getMediaThumbnail',MinidomNode_getMediaThumbnail)


def addslashes(s):
    if type(s) in (unicode,str) :
        s = re.sub("(\\\\|'|\")", lambda o: "\\" + o.group(1), s)
        return s.encode('latin1','replace')
    else :
        return str(s)


# Cherche les paramêtres de connection à la base de donnée
if os.path.exists(os.environ['HOME']+'.mythtv/config.xml') :
    xmlConfig = os.environ['HOME']+'.mythtv/config.xml'
if os.path.exists('/etc/mythtv/config.xml') :
    xmlConfig = '/etc/mythtv/config.xml'

logger.info('Utilisation du fichier de configuration %s'%xmlConfig)

domConfig = minidom.parse(xmlConfig)
dbHostName = domConfig.getElementsByTagName('DBHostName')[0].firstChild.data
dbUserName = domConfig.getElementsByTagName('DBUserName')[0].firstChild.data
dbPassword = domConfig.getElementsByTagName('DBPassword')[0].firstChild.data
dbName = domConfig.getElementsByTagName('DBName')[0].firstChild.data
dbPort = domConfig.getElementsByTagName('DBPort')[0].firstChild.data

logger.info('''\
    HostName: %s
    UserName: %s
    Password: %s
    Name: %s
    Port: %s (0=default)'''%(dbHostName,dbUserName,dbPassword,dbName,dbPort)
  )

try:
    dbConn = MySQLdb.connect (
            host = dbHostName,
            user = dbUserName,
            passwd = dbPassword,
            db = dbName,
            port = int(dbPort)
        )
except MySQLdb.Error, e:
    logger.error( "%d: %s" % (e.args[0], e.args[1]))
    sys.exit (1)

dbCursor = dbConn.cursor()
dbCursor.execute ("SELECT DISTINCT name,commandline,type FROM internetcontent WHERE tree=1")
grabbers = dbCursor.fetchall()
for (name,commandline,typegrab) in grabbers :
    logger.info('Execute %s -T ...'%commandline)
    (stdin, stdout, stderr) = os.popen3(grabberDir+commandline+' -T')

    logger.info('Parse xml ...')
    try :
        domGrab = minidom.parse(stdout)
    except :
        logger.warning('  -> Fichier xml invalide, mise à jour ignorée')
    else :
        items = []
        for itemNode in domGrab.getElementsByTagName('item') :
            link = itemNode.getTextByTagName('link')
            mediaContent = itemNode.getMediaContent()
            mediaThumbnail = itemNode.getMediaThumbnail()
            if link != mediaContent['url'] and itemNode.getTextByTagName('player') != '' :
                is_downloadable = '1'
            else :
                is_downloadable = '0'

            items.append({
                    'feedtitle' : name,
                    'path' : itemNode.getPath(),
                    'paththumb' : itemNode.getPathThumb(),
                    'title' : itemNode.getTextByTagName('title'),
                    'subtitle' : itemNode.getTextByTagName('mythtv:subtitle'),
                    'season' : itemNode.getTextByTagName('mythtv:season'),
                    'episode' : itemNode.getTextByTagName('mythtv:episode'),
                    'description' : itemNode.getTextByTagName('description'),
                    'url' : link,
                    'type' : typegrab,
                    'thumbnail' : mediaThumbnail['url'],
                    'mediaURL' : mediaContent['url'],
                    'author' : itemNode.getTextByTagName('author'),
                    'date': itemNode.getDate(),
                    'time': mediaContent['duration'],
                    'rating' : itemNode.getTextByTagName('rating'),
                    'filesize': mediaContent['length'],
                    'player' : itemNode.getTextByTagName('player'),
                    'playerargs' : itemNode.getTextByTagName('playerargs'),
                    'download' : itemNode.getTextByTagName('download'),
                    'downloadargs' : itemNode.getTextByTagName('downloadargs'),
                    'width': mediaContent['width'],
                    'height': mediaContent['height'],
                    'language': mediaContent['lang'],
                    'podcast' : '0',
                    'downloadable' : is_downloadable,
                    'customhtml' : '0',
                    'countries': itemNode.getTextByTagName('mythtv:country')
                })

        logger.info('update database ...')
        if len(items) == 0 :
            logger.warning('  -> Aucun élément a été récupéré, mise à jour ignorée')
        else :
            logger.info('  -> %i éléments ont été récupéré, mise à jour ...'%len(items))
            #efface les ancienne donnée
            dbCursor.execute ("DELETE FROM internetcontentarticles WHERE feedtitle='"+name+"'")
            #insere les nouvelles
            for item in items :
                sql = 'INSERT INTO internetcontentarticles (%s)\n'%','.join(items[0].keys())
                sql += 'VALUES (\n'
                addslashesvalues = []
                for value in item.values() :
                    addslashesvalues.append("'%s'"%addslashes(value))
                sql += ','.join(addslashesvalues)
                sql += ')'

                try :
                    dbCursor.execute(sql)
                except MySQLdb.Error, e:
                    logger.warning(e)
                    logger.warning(sql)

dbConn.close()

